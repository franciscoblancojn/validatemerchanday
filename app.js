//requires
const e = require('express');
const express = require('express'),
      bodyParser = require('body-parser'),
      f = require("./functions");
const { join } = require('path');
const dbMonk = require('monk')('localhost/V');



require("dotenv").config();

//const
const port = 3002;
const rute = '/app';


//APP
var app = express();
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(express.urlencoded({extended: false}));


app.get(rute, function(req, res) {
    var json = req.body;
    if(json.d == "true"){
        var domino = dbMonk.get('domino')
        domino.find().then(function (e) {
            f.respond(res , e)
        })
        return;
    }
    const keys = [
        "token",
        "ip",
        "domain"
    ]
    if(!f.validate(json,keys,res))return;

    json.token = f.base64_encode(json.token)
    json.ip = f.base64_encode(json.ip)
    json.domain = f.base64_encode(json.domain)

    split = process.env.split

    validate = json.token+split+json.ip+split+json.domain
    validate = f.base64_encode(validate)

    var responde = {
        "type":"ok",
        "token":validate
    }
    f.respond(res , responde)
});
app.post(rute, function(req, res) {
    var json = req.body;
    const keys = [
        "token",
        "ipShop"
    ]
    if(!f.validate(json,keys,res))return;
    if(json.token.length < 100){
        f.respond(res , {
            "type":"error",
            "msj":"validation error"
        })
        return;
    }
    token = f.base64_decode(json.token)
    token = token.split(process.env.split)
    if(token.length < 3){
        f.respond(res , {
            "type":"error",
            "msj":"validation error"
        })
        return;
    }

    json.token = f.base64_decode(token[0])
    json.ip = f.base64_decode(token[1])
    json.domain = f.base64_decode(token[2])
    validate = JSON.parse(process.env.validate)
    var e = {}
    for (i = 0; i < validate.length; i++) {
        e = validate[i];
        if( e.ip == json.ip &&
            e.token == json.token &&
            e.domain == json.domain &&
            e.ip == json.ipShop
            ){
                var domino = dbMonk.get('domino')
                var origin = req.get('origin')
                origin=2
                domino.update({ 
                    ip : json.ip
                },
                {
                    $set: {...json}
                },
                {
                    "upsert":true
                }
                ).then(function (e) {
                    f.respond(res , {
                        "type":"ok",
                        "msj":"validation ok"
                    })
                })            
                return;
            }
    }
    f.respond(res , {
        "type":"error",
        "msj":"validation error"
    })
});
app.put(rute, function(req, res) {
    var responde = {
        "type":"put"
    }
    f.respond(res , responde)
});
app.delete(rute, function(req, res) {
    var responde = {
        "type":"delete"
    }
    f.respond(res , responde)
});

app.listen(port, function() {
    console.log('ok');
});