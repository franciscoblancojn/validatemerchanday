var f = {}
f.query = (connection,sql) =>{
    try{
        const result = connection.query(sql);
        return {
            'type':'ok',
            'result':result
        };
    }catch(err){
        return {
            'type':'error',
            'error':err
        };
    }
}

f.respond = (res , responde) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(responde));
    res.flush()
    res.end();
}

f.validate = (json , keys, res) => {
    for (i = 0; i < keys.length; i++) {
        if(json[keys[i]] == null || json[keys[i]] == undefined || json[keys[i]] == ""){
            f.respond(res,{
                "type":"error",
                "msj":`json.${keys[i]} undefined`
            })
            return false;
        }
    }
    return true;
}

f.base64_encode = (e) => {
    return Buffer.from(e).toString('base64')
}
f.base64_decode = (e) => {
    return (new Buffer(e, 'base64')).toString('ascii');
}

module.exports = f;